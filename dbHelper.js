const sqlite3 = require("sqlite3").verbose();

module.exports.initDatabase = (filename) => {
  return new Promise((resolve, reject) => {
    // connects to disk file database
    let db = new sqlite3.Database(filename, (err) => {
      if (err) {
        reject(null);
      }
      resolve(db);
    });
  });
};

module.exports.queryName = (db, name) => {
  return new Promise((resolve, reject) => {
    // query selects data from specified columns from table and uses like operator to search specified pattern
    let sql = `SELECT name, latitude, longitude from locations WHERE name LIKE '${name}%';`;

    const regex = new RegExp("^[A-Za-z0-9 _À-ý'’]*$");

    // verify string contains only numbers, characters and spaces
    if (!regex.test(name)) {
      resolve([]);
    }

    // remove any leading and trailing white space
    name = name.trim();

    // return empty array if query length to short
    if (name.length < 2) {
      resolve([]);
    }

    // performs and returns query
    db.all(sql, [], (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows);
    });
  });
};
