// import API mocking utilities from Mock Service Worker
import { rest } from "msw";
import { setupServer } from "msw/node";

import {
  render,
  screen,
  fireEvent,
  waitFor,
  queryByAttribute,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { App, port, hostname, locationQuery } from "./App";

const server = setupServer(
  rest.get(`http://${hostname}:${port}/locations/`, (req, res, ctx) => {
    return res(ctx.status(400));
  })
);

const getById = queryByAttribute.bind(null, "id");

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("renders enter location name", () => {
  render(<App />);
  const linkElement = screen.getByText(/Enter location name/i);
  expect(linkElement).toBeInTheDocument();
});

test("validate user input box value changes", async () => {
  server.use(
    rest.get(`http://${hostname}:${port}/locations/`, (req, res, ctx) => {
      return res(ctx.status(400));
    })
  );

  const dom = render(<App />);
  const input = getById(dom.container, "username-input");

  userEvent.type(input, "Heswall");
  expect(input).toHaveValue("Heswall");
});

/*
  TODO: write test that validates results lists show on valid request
*/

/*
  TODO: write test that validates error modal shows up
*/

/*
  TODO: write test that validates error modal can be closed
*/
