import React from "react";
import "./App.scss";

export const port =
  !process.env.NODE_ENV || process.env.NODE_ENV === "development"
    ? 3000
    : window.location.port;

export const hostname = window.location.hostname;

export let locationQuery = async (searchQuery) => {
  let response = await fetch(
    `http://${hostname}:${port}/locations/?q=${searchQuery}`
  );
  return response.json();
};

class Error extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.props.handleClick;
  }

  render() {
    return (
      <div className="modal">
        <div className="modal-content">
          <span className="close" onClick={this.handleClick}>
            &times;
          </span>
          <h2 className="error-heading">Error</h2>
          <p className="error-msg">{this.props.errorMessage}</p>
        </div>
      </div>
    );
  }
}

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: "",
      errorMessage: "",
      results: [],
    };
  }

  onSearchQueryChanged = async (e) => {
    this.setState({ searchQuery: e.target.value }, async () => {
      try {
        // queries endpoint with search
        let results = await locationQuery(this.state.searchQuery);

        // unique key to more easily identify which elements have changed in array
        results.forEach((result) => {
          result.id = 1 + Math.random();
        });

        // update results array in state
        this.setState({
          results,
        });
      } catch (e) {
        this.setState({
          errorMessage: "A problem occurred during search query, try again.",
        });
      }
    });
  };

  render() {
    let resultsCount;
    let errorModal;

    // conditionally renders result count if # results > 0
    if (this.state.results.length) {
      resultsCount = (
        <h3 className="results-count">Results: {this.state.results.length}</h3>
      );
    }

    // conditionally renders error modal if error message length > 0
    if (this.state.errorMessage.length) {
      errorModal = (
        <Error
          errorMessage={this.state.errorMessage}
          handleClick={() => {
            this.setState({
              errorMessage: "",
            });
          }}
        />
      );
    }

    return (
      <div>
        {errorModal}

        <h1 className="app-title">Geo Location Search - UK</h1>

        <div className="container">
          <div className="search-input-group">
            <label htmlFor="username-input" className="input-field-title">
              Enter location name
            </label>
            <input
              className="input-field"
              id="username-input"
              type="text"
              placeholder="e.g. London"
              value={this.state.searchQuery}
              onInput={this.onSearchQueryChanged}
              maxLength="250"
            />
          </div>

          {resultsCount}

          <div className="results">
            {this.state.results.map((item) => {
              return (
                <div className="result-card" key={item.id}>
                  <div className="location-name">{item.name}</div>
                  <div className="location-pos">Latitude: {item.latitude}</div>
                  <div className="location-pos">
                    Longitude: {item.longitude}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
