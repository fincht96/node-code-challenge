const express = require("express");
const cors = require("cors");
const dbHelper = require("./dbHelper");
const sortHelper = require("./sortHelper");

const app = express();
const port = 3000;
let db = null;

// in prod enviroment hosts static web site
if (process.env.NODE_ENV === "prod") {
  app.use(express.static("public"));

  // in dev enviroment, cross origin requests supported
} else {
  app.use(cors());
}

// endpoint that accepts location queries with url parameter
app.get("/locations", async (req, res) => {
  try {
    let q = req.query.q;
    let queryMatches = [];

    if (q.length) {
      // url parameter too long
      if (q.length > 250) {
        throw "URL parameter too long";
      }

      // queries database for all possible matches
      queryMatches = await dbHelper.queryName(db, q);

      // if multiple matches sort by closest match
      if (queryMatches.length > 1) {
        queryMatches = sortHelper.sortByClosestMatch(
          req.query.q,
          queryMatches,
          "name"
        );
      }
    }
    res.send(queryMatches);
  } catch (e) {
    res.sendStatus(400);
  }
});

dbHelper
  .initDatabase("./data/locations.db")
  .then((res) => {
    db = res;
    console.log(`SQLite database initialised`);
    app.listen(port, async () => {
      console.log(`App listening at http://localhost:${port}`);
    });
  })
  .catch((e) => {
    console.log("Error initialising database: ", e);
  });

// for testing
module.exports = app;
