// Requiring module
let chai = require("chai");
let chaiHttp = require("chai-http");
const assert = require("assert");
let should = chai.should();
let expect = chai.expect;

const dbHelper = require("../dbHelper");
const sortHelper = require("../sortHelper");
let server = require("../index");

chai.use(chaiHttp);

describe("Database testing", () => {
  let db = null;

  describe("Opening database", () => {
    it("Open locations.db file", async () => {
      db = await dbHelper.initDatabase("./data/locations.db");
      assert.notStrictEqual(db, null);
    });
  });

  describe("Querying database", () => {
    it("Find query match for name with only ascii characters, 'Hastings Castle'", async () => {
      let res = await dbHelper.queryName(db, "Hastings Castle");
      assert.strictEqual(res[0].name, "Hastings Castle");
    });

    it("Find query match for name with non-ascii characters 'Pýrgos Xylofágou'", async () => {
      let res = await dbHelper.queryName(db, "Pýrgos Xylofágou");
      assert.strictEqual(res[0].name, "Pýrgos Xylofágou");
    });

    it("Ensure latitude is returned in query match'", async () => {
      let res = await dbHelper.queryName(db, "Hastings Castle");
      assert.strictEqual(res[0].latitude, "50.8559");
    });

    it("Ensure longitude is returned in query match'", async () => {
      let res = await dbHelper.queryName(db, "Hastings Castle");
      assert.strictEqual(res[0].longitude, "0.58506");
    });

    it("Return no query match if name query length < 2", async () => {
      let res = await dbHelper.queryName(db, "H");
      assert.strictEqual(res.length, 0);
    });

    it("Return no query match if query length is empty", async () => {
      let res = await dbHelper.queryName(db, "");
      assert.strictEqual(res.length, 0);
    });

    it("Return no query match if query length is just space characters", async () => {
      let res = await dbHelper.queryName(db, "   ");
      assert.strictEqual(res.length, 0);
    });

    it("Return no query match if special characters are included", async () => {
      let res = await dbHelper.queryName(db, "%^;'@");
      assert.strictEqual(res.length, 0);
    });
  });
});

describe("Sorting algorithm testing", () => {
  it("sort closest to string, 'hel'", async () => {
    let array = ["heliscoop", "hel", "heliman", "heliport", "helicopter"];

    let res = sortHelper.sortByClosestMatch("hel", array);

    expect(res).deep.to.equal([
      "hel",
      "heliman",
      "heliport",
      "heliscoop",
      "helicopter",
    ]);
  });

  it("sort closest to string, empty array", async () => {
    let res = sortHelper.sortByClosestMatch("hel", []);
    expect(res).deep.to.equal([]);
  });

  it("sort closest to string, array of objects", async () => {
    let array = [
      { name: "heliscoop" },
      { name: "hel" },
      { name: "heliman" },
      { name: "heliport" },
      { name: "helicopter" },
    ];

    let res = sortHelper.sortByClosestMatch("hel", array, "name");
    expect(res).deep.to.equal([
      { name: "hel" },
      { name: "heliman" },
      { name: "heliport" },
      { name: "heliscoop" },
      { name: "helicopter" },
    ]);
  });
});

describe("Server endpoint testing", () => {
  describe("/locations?q=Heswall", () => {
    it("it should GET all locations that contain Heswall", (done) => {
      chai
        .request(server)
        .get("/locations?q=Heswall")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.length.should.be.eql(1);
          done();
        });
    });
  });

  describe("/locations?q=London", () => {
    it("it should GET all locations that contain London", (done) => {
      chai
        .request(server)
        .get("/locations?q=London")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.length.should.be.eql(61);
          done();
        });
    });
  });

  describe("/locations?q=", () => {
    it("it should GET empty array", (done) => {
      chai
        .request(server)
        .get("/locations?q=")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/locations?q={251 CHARACTERS}", () => {
    it("it should return error 400", (done) => {
      let urlParam = new Array(251).fill("h").join("");

      chai
        .request(server)
        .get(`/locations?q=${urlParam}`)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/locations?d=Heswall", () => {
    it("it should return error 400", (done) => {
      chai
        .request(server)
        .get("/locations?d=Heswall")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/locations", () => {
    it("it should return error 400", (done) => {
      chai
        .request(server)
        .get("/locations")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});
