const levenshtein = require("js-levenshtein");

module.exports.sortByClosestMatch = (val, array, field = null) => {
  // calculates how close each element in array is to val
  let newArray = array.map((el) => {
    if (field === null) {
      return [levenshtein(val, el), el];
    }
    return [levenshtein(val, el[field]), el];
  });

  function compare(a, b) {
    if (a[0] < b[0]) {
      return -1;
    }
    if (a[0] > b[0]) {
      return 1;
    }
    return 0;
  }

  // orders by closest match
  newArray = newArray.sort(compare);

  newArray = newArray.map((el) => {
    return el[1];
  });

  return newArray;
};
