# Questions

Qs1: Explain the output of the following code and why

```js
setTimeout(function () {
  console.log("1");
}, 100);
console.log("2");
```

As1: The character '2' will be immediately printed to the console and then 100 ms later the character '1' will be printed. The setTimeout function is used to call a function with a specified delay period.

Qs2: Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

As2: This is an example of a recursive function, each function call will call itself while the condition is met i.e. d remains less than 10. After this has occurred each function will complete it's execution in LIFO order, i.e. printing the passed in parameter to the console. This will result in sequentially printed output from 10 down to 0.

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

As3: The issue with this function is that if the parameter d evaluates to false e.g. no parameter is provided then 5 will be printed, however some values e.g. null, 0 and `` will evaluate to false and as such in these scenarios the function will still print 5.

Qs4: Explain the output of the following code and why

```js
function foo(a) {
  return function (b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

As4: JavaScript treats functions as first class citizens, that is they can be assigned to variables and passed as parameters to other functions.
In this example when the function foo is invoked it returns an anonymous function that accepts a parameter. So when foo is called with parameter 1 the returned anonymous function, with a in its context set to 1, is assigned to the variable bar. When bar is then invoked with parameter 2 the variable b is assigned the value of 2 and returns the value a + b which in this case is 3. The result of 3 is then printed to the console.

Qs5: Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function () {
    done(a * 2);
  }, 100);
}
```

As5: This is an example of a callback function and will typically be used for the execution of long running tasks. When the task is completed the passed in callback function will be invoked and the result is passed as a parameter.
